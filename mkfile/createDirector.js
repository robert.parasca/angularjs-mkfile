const fs = require('fs');

/**
 * @pathArv = string = /core/test - location for the new folder to be created
 * @name = string = test-page - name of the new folder to be created
 */

const createDirector = (pathArgv, name) => {

    // first: check if the path from the terminal is correct.
    //                      /root/wtf/resources/js + /core/pages/
    const completePath = __defaultPathForDirectory + formatStartOfPath(pathArgv);

    if(checkIfDirectorExists(completePath)) {

        // then add the name and check if the pathArgv/name exists or not.
        //          /root/wtf/resources/js/core/page. + test-page
        const temp = completePath + name;

        if(!checkIfDirectorExists(temp)) {
            fs.mkdir(temp, (err) => {
                return false;
            });
            return temp;
        } else {
            console.log('already exists');
            return false;
        }
    }
    
    return false;
}

const formatStartOfPath = (path) => {
    const pathArr = path.split('');
    
    // for the use case when --path="core/something/" instead of --path="/core/something"
    if(pathArr[0] !== '/') {
        pathArr.unshift('/');
    }
    
    // for the use case when --path="/core/something" instead of --path="/core/something/"
    if(pathArr[pathArr.length - 1] !== '/') {
        pathArr.push('/');
    }

    const pathStr = pathArr.join('');

    return pathStr;
    // return path;
}

const checkIfDirectorExists = (path) => {
    return fs.existsSync(path);
}

module.exports.createDirector = createDirector;