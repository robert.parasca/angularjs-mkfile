const serviceWannaBe = (name) => {
    return `class ${name}Service {
    constructor() {
        console.log('${name} service is working.');
    }
}

export default ${name}Service;
    `;
};

module.exports.serviceWannaBe = serviceWannaBe;