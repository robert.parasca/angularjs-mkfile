const routesWannaBe = (name) => {
    name = name.toLowerCase();
    return `class ${name}Routes {
    constructor($stateProvider) {
        $stateProvider.state('HERE', {
            pageTitle: '${name}',
            url: 'HERE',
            template: require('./${name}.pug'),
            controller: '${name}Ctrl',
            controllerAs: '$${name}Ctrl',
            resolve: {
                fct: /* @ngInject */ (Middleware) => {
                    return Middleware.canSee('HERE');
                },
            },
            onEnter: /* @ngInject */ (Middleware, Auth, $q, $state) => {
                const deferred = $q.defer();
                if (Middleware.hasPermissionToSee('HERE')) {
                    deferred.resolve();
                }
                else {
                    $state.go('404');
                }
                return deferred.promise;
            }
        });
    }

    /* @ngInject */
    static routesFactory($stateProvider) {
        ${name}Routes.instance = new ${name}Routes($stateProvider);
        return ${name}Routes.instance;
    }
}

export default ${name}Routes;
    `;
};

module.exports.routesWannaBe = routesWannaBe;