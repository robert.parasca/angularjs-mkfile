const controllerWannaBe = (name) => {
    name = name.toLowerCase();
    return `class ${name}Ctrl {
    constructor() {
        console.log('${name} controller is working.');
    }
}

/* @ngInject */
export default ${name}Ctrl;
    `;
};

module.exports.controllerWannaBe = controllerWannaBe;