const indexJSWannaBe = (name) => {
    name = name.toLowerCase();
    return `const ${name}Module = angular.module('HERE.${name}', []);

import ${name}Routes from './${name}.routes';
${name}Module.config(${name}Routes.routesFactory);

import ${name}Ctrl from './${name}.controller';
${name}Module.controller('${name}Ctrl', ${name}Ctrl);

export default ${name}Module;
    `;
};

module.exports.indexJSWannaBe = indexJSWannaBe;