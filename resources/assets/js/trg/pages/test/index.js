const testModule = angular.module('HERE.test', []);

import testRoutes from './test.routes';
testModule.config(testRoutes.routesFactory);

import testCtrl from './test.controller';
testModule.controller('testCtrl', testCtrl);

export default testModule;
    