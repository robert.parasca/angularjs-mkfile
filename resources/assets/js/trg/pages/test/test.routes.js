class testRoutes {
    constructor($stateProvider) {
        $stateProvider.state('HERE', {
            pageTitle: 'test',
            url: 'HERE',
            template: require('./test.pug'),
            controller: 'testCtrl',
            controllerAs: '$testCtrl',
            resolve: {
                fct: /* @ngInject */ (Middleware) => {
                    return Middleware.canSee('HERE');
                },
            },
            onEnter: /* @ngInject */ (Middleware, Auth, $q, $state) => {
                const deferred = $q.defer();
                if (Middleware.hasPermissionToSee('HERE')) {
                    deferred.resolve();
                }
                else {
                    $state.go('404');
                }
                return deferred.promise;
            }
        });
    }

    /* @ngInject */
    static routesFactory($stateProvider) {
        testRoutes.instance = new testRoutes($stateProvider);
        return testRoutes.instance;
    }
}

export default testRoutes;
    