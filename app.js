/*
*   !!! WORK IN PROGRESS !!!
*       Notes
*   1. This file must be in the root folder of the project.
*   2. This utility is used to create custom files for a project using AngularJS.
*/

// mkfile --module=test =>
// test.controller.js
// test.routes.js
// test.pug
// index.js

/*
*  controller
*  route
*  pug
*  index.js
*/

// what if the director already exists? - fs.existsSync()
// what if the path is not correct?

// create the path (custom made for my project)

global.__defaultPathForDirectory = `${__dirname}/resources/assets/js`;

const yargs = require('yargs');
const fs = require('fs');

const createDirector = require('./mkfile/createDirector').createDirector;

// types
const controllerWannaBe = require('./mkfile/types/controller').controllerWannaBe;
const routesWannaBe = require('./mkfile/types/routes').routesWannaBe;
const pugWannaBe = require('./mkfile/types/pug').pugWannaBe;
const indexJSWannaBe = require('./mkfile/types/indexJS').indexJSWannaBe;
const serviceWannaBe = require('./mkfile/types/service').serviceWannaBe;

// object with the arguments from the command line 
// todo: validate the arguments.

const argv = yargs.argv;

const newDirResult = createDirector(argv.path, argv.name);

console.log(newDirResult);

// create files inside the director

const createFile = (fileType, name) => {
    let fullName = '';
    let data = '';
    switch(fileType) {
        case 'controller':
            fullName = `${name}.controller.js`;
            data = controllerWannaBe(argv.name);
            break;
        case 'routes':
            fullName = `${name}.routes.js`;
            data = routesWannaBe(argv.name);
            break;
        case 'pug':
            fullName = `${name}.pug`;
            data = pugWannaBe(argv.name);
            break;
        case 'index':
            fullName = `index.js`;
            data = indexJSWannaBe(argv.name);
            break;
        case 'service':
            fullName = `${name}.service.js`;
            data = serviceWannaBe(argv.name);
            break;
        default:
            break;
    }

    fs.writeFileSync(`${newDirResult}/${fullName}`, data, (err) => {
        return false;
    });

    return true;
}

// parse the command and call the functions

const execute = (option) => {
    switch(option) {
        case 'module':
            createFile('controller', argv.name);
            createFile('routes', argv.name);
            createFile('pug', argv.name);
            createFile('index', argv.name);
            break;
        default:
            break;
    }
}

if(newDirResult) {
    execute(argv.option);
}
